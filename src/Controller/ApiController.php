<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{
    private $doctrine;
    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
    }
    #[Route('/', name: 'api')]
    public function index(): Response
    {
        return $this->render('api/index.html.twig');
    }
    
    #[Route("/api/v1.0/get-servers", methods: "GET")]
    public function servers(Request $request): Response
    {
        $params = $request->query->all();
        $response = $this->doctrine->getRepository('App:Server')->getSearchData($params);
        
        return new JsonResponse($response);
    }
}
