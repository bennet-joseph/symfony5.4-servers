<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;

class LocationController
{
    #[Route('/api/v1.0/get-locations', name: 'location')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $locations = $doctrine->getRepository('App:Location')->getAllLocations();
        return new JsonResponse($locations);
    }
}
