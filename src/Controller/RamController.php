<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Persistence\ManagerRegistry;

class RamController
{
    #[Route('/api/v1.0/get-ram-sizes', name: 'ram')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $ramSizes = $doctrine->getRepository('App:Ram')->getAllRamSizes();
        return new JsonResponse($ramSizes);
    }
}
