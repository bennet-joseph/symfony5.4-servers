<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;


class StorageController
{
    #[Route("/api/v1.0/get-storage-types", name: "storage")]
    public function index(ManagerRegistry $doctrine): Response
    {

        $hddTypes = $doctrine->getRepository('App:HardDiskType')->getAllHardDiskTypes();
        return new JsonResponse($hddTypes);
    }
}
