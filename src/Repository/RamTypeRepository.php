<?php

namespace App\Repository;

use App\Entity\RamType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RamType|null find($id, $lockMode = null, $lockVersion = null)
 * @method RamType|null findOneBy(array $criteria, array $orderBy = null)
 * @method RamType[]    findAll()
 * @method RamType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RamTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RamType::class);
    }

    // /**
    //  * @return RamType[] Returns an array of RamType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RamType
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
