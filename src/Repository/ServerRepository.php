<?php

namespace App\Repository;

use App\Entity\Server;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Server|null find($id, $lockMode = null, $lockVersion = null)
 * @method Server|null findOneBy(array $criteria, array $orderBy = null)
 * @method Server[]    findAll()
 * @method Server[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Server::class);
    }

    /**
     * @return Server[] Returns an array of Server objects
     */
    
    public function getSearchQuery($params)
    {
        if(isset($params['hdd_start']) && isset($params['hdd_end'])) {
            $hddStart = (int)str_replace(['GB', 'TB'], '', $params['hdd_start']) * (strpos($params['hdd_start'], 'TB')?1024000:1024);
            $hddEnd = (int)str_replace(['GB', 'TB'], '', $params['hdd_end']) * (strpos($params['hdd_end'], 'TB')?1024000:1024);
        }
        
        $qb = $this->createQueryBuilder('s');
        $qb = $qb->select("s.id, s.name AS server_name, l.location AS l_location, s.currency, s.price AS price")
                ->addSelect("CONCAT(r.capacity,' ',rt.name) AS ram")
                ->addSelect("CONCAT(sh.hardDiskCount,'X',h.capacity,' ',h.measurement,' ',hdt.name) AS hdd")
                ->addSelect("(sh.hardDiskCount * h.capacity * IF(h.measurement = 'TB',1024000,1024)) AS total_hdd")
                ->join('s.location', 'l')
                ->join('s.serverRams', 'sr')
                ->join('sr.ram', 'r')
                ->join('r.type', 'rt')
                ->join('s.serverHdd', 'sh')
                ->join('sh.hardDisk', 'h')
                ->join('h.type', 'hdt');
        
        
        if(isset($params['location']) && $params['location'] != '') {
            $qb = $qb->andWhere('s.location = :location')
                    ->setParameter('location', $params['location']);
        }
        
        if(isset($params['storage_type']) && $params['storage_type'] != '') {
            $qb = $qb->andWhere('hdt.name = :storageType')
                    ->setParameter('storageType', $params['storage_type']);
        }
        
        if(isset($params['ram_size']) && $params['ram_size'] != '') {
        $params['ram_size']  = explode (',', $params['ram_size']);
            $qb = $qb->andWhere('r.capacity IN(:ramSize)')
                    ->setParameter('ramSize', $params['ram_size']);
        }
        
        if(isset($params['hdd_start']) && isset($params['hdd_end'])) {
            $qb = $qb->having('total_hdd BETWEEN ' . $hddStart . ' AND ' . $hddEnd);
        }
//        $order = explode(' ', $params['order']);
//        $qb = $qb->orderBy($order[0], $order[1]);
        $qb = $qb->orderBy('s.name', 'ASC');
//        $query = $qb->getQuery();
//        echo $query->getSql();
        return $qb;
    }
    
    public function getSearchData($params) {
        try {
            $dql = $this->getSearchQuery($params);
            $countDql = clone $dql;
            $total =  count($countDql->getQuery()->getResult());
            $pages = 1;
            $prevPageUrl = null;
            $nextPageUrl = null;
            $result = [];
            if(!isset($params['offset']) || !is_numeric($params['offset'])) {
                $params['offset'] = 0;
            }
            if(!isset($params['limit']) || !is_numeric($params['limit'])) {
                $params['limit'] = 10;
            }
            $dql = $dql->setFirstResult($params['offset'])
                      ->setMaxResults($params['limit']);
            $query = $dql->getQuery();
            //echo $query->getSql();
            $result = $query->getResult();
            $metadataEnd = $params['offset'] + $params['limit'];
            $metadataEnd = ($metadataEnd > $total?$total:$metadataEnd);
            $metaData = [
                'records_start' => $params['offset'],
                'records_end' => $metadataEnd,
                'records_total' => $total
            ];
            if(isset($params['offset']) && is_numeric($params['offset']) && isset($params['limit']) && is_numeric($params['limit'])) {
                $pages = ceil($total / $params['limit']);
                $pageUrl = '/api/v1.0/get-servers?';
                $startIndex = $params['offset'];
                if($params['offset'] > 0) {
                    $prevPage = ($startIndex > $params['limit']?($startIndex - $params['limit']):0);
                    $params['offset'] = $prevPage;
                    $prevPageUrl = $pageUrl . http_build_query($params);
                }
                if($total > $startIndex + $params['limit']) {
                    $params['offset'] = $startIndex + $params['limit'];
                    $nextPageUrl = $pageUrl . http_build_query($params);
                }
            }
            $response = [
                'StatusCode' => 200,
                'metadata' => $metaData,
                'TotalRecordCount' => $total,
                'TotalPages' => $pages,
                'PrevPage' => $prevPageUrl,
                'NextPage' => $nextPageUrl,
                'Records' => $result
            ];
        } catch(\Exception $e) {
            $response = [
                'StatusCode' => 400,
                'Message' => $e->getMessage()
            ];
        } finally {
            return $response;
        }
    }
}
