<?php

namespace App\Repository;

use App\Entity\HardDiskType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HardDiskType|null find($id, $lockMode = null, $lockVersion = null)
 * @method HardDiskType|null findOneBy(array $criteria, array $orderBy = null)
 * @method HardDiskType[]    findAll()
 * @method HardDiskType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HardDiskTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HardDiskType::class);
    }

    /**
     * @return HardDiskType[] Returns an array of HardDiskType objects
     */
    
    public function getAllHardDiskTypes()
    {
        return $this->createQueryBuilder('h')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)
        ;
    }
}
