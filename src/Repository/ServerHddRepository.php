<?php

namespace App\Repository;

use App\Entity\ServerHdd;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ServerHdd|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServerHdd|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServerHdd[]    findAll()
 * @method ServerHdd[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServerHddRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServerHdd::class);
    }

    // /**
    //  * @return ServerHdd[] Returns an array of ServerHdd objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ServerHdd
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
