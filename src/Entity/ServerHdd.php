<?php

namespace App\Entity;

use App\Repository\ServerHddRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ServerHddRepository::class)]
class ServerHdd
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity:"App\Entity\Server", inversedBy:"ServerHdd")]
    private $server;

    #[ORM\ManyToOne(targetEntity:"App\Entity\HardDisk", inversedBy:"ServerHdd")]
    private $hardDisk;

    #[ORM\Column(type: 'integer')]
    private $hardDiskCount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServer(): ?int
    {
        return $this->server;
    }

    public function setServer(int $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function getHardDisk(): ?int
    {
        return $this->hardDisk;
    }

    public function setHardDisk(int $hardDisk): self
    {
        $this->hardDisk = $hardDisk;

        return $this;
    }

    public function getHardDiskCount(): ?int
    {
        return $this->hardDiskCount;
    }

    public function setHardDiskCount(int $hardDiskCount): self
    {
        $this->hardDiskCount = $hardDiskCount;

        return $this;
    }
}
