<?php

namespace App\Entity;

use App\Repository\ServerRamRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ServerRamRepository::class)]
class ServerRam
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    
    #[ORM\ManyToOne(targetEntity: "App\Entity\Server", inversedBy: "ServerRams")]
    private $server;

    #[ORM\ManyToOne(targetEntity: "App\Entity\Ram", inversedBy: "ServerRams")]
    private $ram;

    #[ORM\Column(type: "integer")]
    private $ramCount;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getServer(): ?int
    {
        return $this->server;
    }

    public function setServer(int $server): self
    {
        $this->server = $server;

        return $this;
    }

    public function getRam(): ?int
    {
        return $this->ram;
    }

    public function setRam(int $ram): self
    {
        $this->ram = $ram;

        return $this;
    }

    public function getRamCount(): ?int
    {
        return $this->ramCount;
    }

    public function setRamCount(int $ramCount): self
    {
        $this->ramCount = $ramCount;

        return $this;
    }
}
