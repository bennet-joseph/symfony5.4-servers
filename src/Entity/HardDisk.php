<?php

namespace App\Entity;

use App\Repository\HardDiskRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HardDiskRepository::class)]
class HardDisk
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    private $id;

    #[ORM\Column(type: "integer")]
    private $capacity;

    #[ORM\Column(type: "string", length: 255)]
    private $measurement;

    #[ORM\ManyToOne(targetEntity: "App\Entity\HardDiskType", inversedBy: "HardDisks")]
    private $type;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getMeasurement(): ?string
    {
        return $this->measurement;
    }

    public function setMeasurement(string $measurement): self
    {
        $this->measurement = $measurement;

        return $this;
    }
}
