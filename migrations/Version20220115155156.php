<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220115155156 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE hard_disk (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, capacity INT NOT NULL, measurement VARCHAR(255) NOT NULL, INDEX IDX_6212417DC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hard_disk_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, location VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ram (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, capacity VARCHAR(255) NOT NULL, INDEX IDX_E7D1222FC54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ram_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE server (id INT AUTO_INCREMENT NOT NULL, location_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, currency VARCHAR(255) NOT NULL, price NUMERIC(10, 0) NOT NULL, INDEX IDX_5A6DD5F664D218E (location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE server_hdd (id INT AUTO_INCREMENT NOT NULL, server_id INT DEFAULT NULL, hard_disk_id INT DEFAULT NULL, hard_disk_count INT NOT NULL, INDEX IDX_CC0242321844E6B7 (server_id), INDEX IDX_CC02423220B3315D (hard_disk_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE server_ram (id INT AUTO_INCREMENT NOT NULL, server_id INT DEFAULT NULL, ram_id INT DEFAULT NULL, ram_count INT NOT NULL, INDEX IDX_D91828751844E6B7 (server_id), INDEX IDX_D91828753366068 (ram_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE hard_disk ADD CONSTRAINT FK_6212417DC54C8C93 FOREIGN KEY (type_id) REFERENCES hard_disk_type (id)');
        $this->addSql('ALTER TABLE ram ADD CONSTRAINT FK_E7D1222FC54C8C93 FOREIGN KEY (type_id) REFERENCES ram_type (id)');
        $this->addSql('ALTER TABLE server ADD CONSTRAINT FK_5A6DD5F664D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE server_hdd ADD CONSTRAINT FK_CC0242321844E6B7 FOREIGN KEY (server_id) REFERENCES server (id)');
        $this->addSql('ALTER TABLE server_hdd ADD CONSTRAINT FK_CC02423220B3315D FOREIGN KEY (hard_disk_id) REFERENCES hard_disk (id)');
        $this->addSql('ALTER TABLE server_ram ADD CONSTRAINT FK_D91828751844E6B7 FOREIGN KEY (server_id) REFERENCES server (id)');
        $this->addSql('ALTER TABLE server_ram ADD CONSTRAINT FK_D91828753366068 FOREIGN KEY (ram_id) REFERENCES ram (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE server_hdd DROP FOREIGN KEY FK_CC02423220B3315D');
        $this->addSql('ALTER TABLE hard_disk DROP FOREIGN KEY FK_6212417DC54C8C93');
        $this->addSql('ALTER TABLE server DROP FOREIGN KEY FK_5A6DD5F664D218E');
        $this->addSql('ALTER TABLE server_ram DROP FOREIGN KEY FK_D91828753366068');
        $this->addSql('ALTER TABLE ram DROP FOREIGN KEY FK_E7D1222FC54C8C93');
        $this->addSql('ALTER TABLE server_hdd DROP FOREIGN KEY FK_CC0242321844E6B7');
        $this->addSql('ALTER TABLE server_ram DROP FOREIGN KEY FK_D91828751844E6B7');
        $this->addSql('DROP TABLE hard_disk');
        $this->addSql('DROP TABLE hard_disk_type');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE ram');
        $this->addSql('DROP TABLE ram_type');
        $this->addSql('DROP TABLE server');
        $this->addSql('DROP TABLE server_hdd');
        $this->addSql('DROP TABLE server_ram');
    }
}
