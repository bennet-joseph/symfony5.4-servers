function Symfony54Servers() {
    this.initialize();
}
Symfony54Servers.prototype = {
    storageValues: ['0', '250GB', '500GB', '1TB', '2TB', '3TB', '4TB', '8TB', '12TB', '24TB', '48TB', '72TB'],
    start: 0,
    size: 10,
    previousPage: null,
    nextPage: null,
    totalPages: 1,
    initialize: function () {
        this.filters();
        this.serversList();
    },
    resetInitialValues: function () {
        this.start = 0;
        this.totalPages = 1;
        this.previousPage = null;
        this.nextPage = null;
        $(".jtable-page-list > span").addClass('jtable-page-number-disabled');
        $(".jtable-page-info").html('');
    },
    getLocations: function () {
        var self = this;
        $.ajax({
            url: '/api/v1.0/get-locations',
            dataType: 'json',
            success: function (data) {
                var option = '<option value="">All Locations</option>';
                $(data).each(function(i, v) {
                    option += '<option value="' + v.id + '">' + v.location + '</option>'
                });
                $("#locations").html(option).change(function () {
                    self.resetInitialValues();
                    self.serversList();
                });
            }
        });
    },
    getStorageTypes: function () {
        var self = this;
        $.ajax({
            url: '/api/v1.0/get-storage-types',
            dataType: 'json',
            success: function (data) {
                var option = '<option value="">All Types</option>';
                $(data).each(function(i, v) {
                    option += '<option value="' + v.name + '">' + v.name + '</option>'
                });
                $("#storage_type").html(option).change(function () {
                    self.resetInitialValues();
                    self.serversList();
                });
            }
        });
    },
    getRamSizes: function () {
        var self = this;
        $.ajax({
            url: '/api/v1.0/get-ram-sizes',
            dataType: 'json',
            success: function (data) {
                var option = '';
                $(data).each(function(i, v) {
                    option += '<div class="row">' +
                                '<div class="col-sm-12">' +
                                    '<label><input type="checkbox" class="ram_size" name="ram_size" value="' + v.capacity + '" /> ' + v.capacity + '</label>' +
                                '</div>' +
                            '</div>';
                });
                $("#ram_types").html(option);
                $(".ram_size").change(function () {
                    self.resetInitialValues();
                    self.serversList();
                });
            }
        });
    },
    getStorageSizes: function () {
        var self = this;
        $("#storage").val('0 - 72TB');
        $("#storage_slider").slider({
            range: true,
            min: 0,
            max: 11,
            values: [0, 11],
            slide: function( event, ui ) {
                console.log(ui.values);
                $("#storage").val(self.storageValues[ui.values[0]] + " - " + self.storageValues[ui.values[1]]);
                $("#storage_start").val(self.storageValues[ui.values[0]]);
                $("#storage_end").val(self.storageValues[ui.values[1]]);
            },
            stop: function(event, ui) {
                self.resetInitialValues();
                self.serversList();
            }
        });
    },
    pageSizes: function () {
        var self = this;
        $(".jtable-bottom-panel select").change(function(){
            self.resetInitialValues();
            self.size = $(this).val();
            self.serversList();
        });
    },
    pagination: function () {
        var self = this;
        $(".jtable-page-list > span").click(function (){
            if($(this).hasClass('jtable-page-number-disabled')) {
                return;
            }
            if($(this).hasClass('jtable-page-number-first')) {
                self.resetInitialValues();
                self.serversList();
            }
            if($(this).hasClass('jtable-page-number-previous')) {
                self.serversList(self.previousPage);
            }
            if($(this).hasClass('jtable-page-number-next')) {
                self.serversList(self.nextPage);
            }
            if($(this).hasClass('jtable-page-number-last')) {
                self.start = (self.totalPages - 1) * self.size;
                self.serversList();
            }
        });
    },
    filters: function () {
        var self = this;
        self.getLocations();
        self.getStorageTypes();
        self.getRamSizes();
        self.getStorageSizes();
        self.pageSizes();
        self.pagination();
    },
    serversList: function (url) {
        var self = this;
        if('undefined' == typeof url) {
            var query = {};
            query['hdd_start'] = $("#storage_start").val();
            query['hdd_end'] = $("#storage_end").val();
            query['ram_size'] = '';
            if($(".ram_size:checked").length > 0) {
                var ramSize = [];
                $(".ram_size:checked").each(function(i,v){
                    ramSize.push($(v).val());
                });
                query['ram_size'] = ramSize.join(',');
            }
            query['storage_type'] = $("#storage_type").val();
            query['location'] = $("#locations").val();
            query['offset'] = self.start;
            query['limit'] = self.size;
            url = '/api/v1.0/get-servers?' + new URLSearchParams(query).toString();
        }
        $(".jtable-busy-message, .jtable-busy-panel-background").show();
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (data) {
                var option = '';
                $(".jtable-busy-message, .jtable-busy-panel-background").hide();
                if(data.StatusCode == 200 && data.Records.length > 0) {
                    self.previousPage = data.PrevPage;
                    self.nextPage = data.NextPage;
                    self.totalPages = data.TotalPages;
                    if(data.PrevPage == null) {
                        $(".jtable-page-number-first").addClass('jtable-page-number-disabled');
                        $(".jtable-page-number-previous").addClass('jtable-page-number-disabled');
                    } else {
                        $(".jtable-page-number-first").removeClass('jtable-page-number-disabled');
                        $(".jtable-page-number-previous").removeClass('jtable-page-number-disabled');
                    }
                    if(data.NextPage == null) {
                        $(".jtable-page-number-next").addClass('jtable-page-number-disabled');
                        $(".jtable-page-number-last").addClass('jtable-page-number-disabled');
                    } else {
                        $(".jtable-page-number-next").removeClass('jtable-page-number-disabled');
                        $(".jtable-page-number-last").removeClass('jtable-page-number-disabled');
                    }
                    $(".jtable-page-info").html('Matching Records: ' + data.TotalRecordCount);
                    $(data.Records).each(function(i, v) {
                        option += '<tr class="jtable-data-row ' + (i%2?'':'jtable-row-even') + '" data-record-key="' + v.id + '">' +
                                        '<td>' + v.server_name + '</td>' + 
                                        '<td>' + v.ram + '</td>' + 
                                        '<td>' + v.hdd + '</td>' + 
                                        '<td>' + v.l_location + '</td>' + 
                                        '<td>' + v.currency + v.price + '</td>' + 
                                    '</tr>';
                    });
                    $("table.jtable tbody").html(option);
                } else {
                    self.resetInitialValues();
                    $("table.jtable tbody").html('<tr class="jtable-no-data-row"><td colspan="5">No data available!</td></tr>');
                }
            }
        });
    }
};
$(document).ready(function () {
    window.symfony54Servers = new Symfony54Servers();
});