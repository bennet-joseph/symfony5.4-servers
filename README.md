## Note

This is a sample project using Symfony version 5.4 to demonstrate my Symfony/ UI skills.

## Setup

Please clone this repository and then


** Step 1: **

Do a composer install

** Step 2: **

You need to set the database credentials in .env file

** Step 3: ** 

Run the below command to create database

```
php bin/console doctrine:database:create
```

** Step 4: **

Run the below command to create tables inside the database


```
php bin/console doctrine:schema:update --force
```
I have the sample data from the excel file converted as SQL. They are available in data.sql file. 
I have not created fixtures since it is not in the scope of this test.

** Step 5: **

Start the web server by using

```
symfony server:start
```

## Details

I have used Symfony5.4 to build this application.

After starting the web server, you should be able to access the application at the URL: [http://127.0.0.1:8000/](http://127.0.0.1:8000/) This is the frontend page to call the API.

I have used bootstrap for this UI, and jTable to call the API and render the result. The hard disc slider control is generated using jQuery UI

## API

The URL for the API is: http://127.0.0.1:8000/api/v1.0/get-servers.
This API returns the paginated result.

The parameters are:

hdd_start: (Eg: 250GB)

hdd_end (Eg: 72TB)

ram_size: (Optional. A comma separated string of values: eg:4GB,16GB)

location: (Optional. Numeric. The possible values can be obtained from another API, http://127.0.0.1:8000/api/v1.0/get-locations)

storage_type: (Optional. String. Value could be one of SATA2, SSD, or SAS)
